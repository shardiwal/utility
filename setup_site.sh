#!/bin/bash
# Duplicate the base vhost template, will need SUDO!!!
sudo -S true

project=$1

echo "Creating a vhost for $project with a webroot in /var/www/$project"
sudo cp template-pub "/etc/apache2/sites-available/$project.conf"

sudo sed -i 's/template/'$project'/g' "/etc/apache2/sites-available/$project.conf"

sudo a2ensite "$project.conf"
sudo service apache2 reload


